window.addEventListener('load', function () {
    document.getElementById('testButton').addEventListener('click', function (e) {
        ga && ga('send', 'event', 'Button Interaction', 'Click', e.target.innerText);
    });

    var mainContent = document.getElementById('mainContent');

    mainContent.addEventListener('click', function (e) {
        var sayItButtonSelector = '#testModal .modal-footer .btn.btn-primary';
        var modalButton = document.querySelector(sayItButtonSelector);

        if (modalButton === e.target || e.target.closest(sayItButtonSelector)) {
            ga && ga('send', 'event', 'Say Hello', 'Say It!', document.getElementById('say').value);
        }
    });
});
