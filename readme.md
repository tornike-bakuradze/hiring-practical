## Welcome to our Hiring Practical!

# Getting Started

1. Ensure you have Docker Installed [Get Docker](https://docs.docker.com/get-docker/)
2. Grab our Test Stack
3. Clone our Test Stack to Local `git clone git@github.com:wheelhousedev/hiring-practical.git`
4. Stand-Up Wordpress `docker-compose up`
5. Get the Name of your Container `docker ps`
6. Copy Child Theme to Docker `docker cp  YOUR-PATH/wp-content/themes/twentytwentyone-child YOUR-CONTAINER-NAME:/bitnami/wordpress/wp-content/themes/.`
7. Enable your new Child Theme in the Admin:  [http://localhost/wp-admin](http://localhost/wp-admin)
    - Username: candidate
    - Password: ThankYouFor@YourInterest!
8. Navigate to [https://localhost/](https://localhost/) and follow the remaining instructions!
9. Once your complete reach out for a review with Dustin!
